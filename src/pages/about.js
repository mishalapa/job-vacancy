import React from "react"
import Header from "./Header"

const About = () => {
  return (
    <div>
      <Header />
      <div className="container">
        <h1>Golang / Go job board</h1>
        <div>
          <p className="title__about">What is Golangprojects</p>
          <p className="text__about">
            The Golang Job market is thriving. When we started this website in
            2014, there were not many job ads that listed golang as a required
            (or even nice to have) skill. Now in October 2022 there are
            considerably more Golang jobs out there. At Golangprojects you will
            find the best and high quality open positions (freelance and
            permanent) for Go / Golang developers / DevOps / engineers. If you
            are a job seeker, looking for a remote job, or if you prefer to work
            onsite, as long as you are interested in putting your Golang skills
            into practise, make sure you follow us on Twitter and sign up to our
            golang jobs newsletter to get the lastest Go jobs.
          </p>
        </div>
        <div>
          <p className="title__about">Why Golangprojects?</p>
          <p className="text__about">
            We focus on quality rather than quanitity, the job posts are not
            scraped from other sites so the information is current (at the time
            of posting), and no additional information are scraped; company
            information and salaries are all provided at the time to make sure
            there are no disappointments for the job hunters.
          </p>
        </div>
        <div>
          <p className="title__about">Contact and Terms of Service</p>
          <p className="text__about">
            Terms of Service for the website. If you need to delete a job ad,
            please use use this form and it will be processed asap. (or if you
            need to contact us for some other reason). Golangprojects belongs to
            UID Software Solutions Ltd, a company based on the Isle of Man. It
            was created by Carl (the founder of UID Software Solutions Ltd) back
            in 2014, you can read more about him over here: Carl's personal
            website. Say thank you with coffee Golangprojects has provided
            golang jobs and go developer profiles for over 8.5 years now! Do you
            want to say "thanks"? Click the button below to buy me a coffee or
            two; coffee in code out!
          </p>
        </div>
      </div>
    </div>
  )
}

export default About
