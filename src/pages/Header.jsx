import { Button, PageHeader } from "antd"
import { Link } from "gatsby"
import React from "react"

const Header = () => (
  <div className="header">
    <div className="header__title">
      <Link to={`/`}>
        <p
          className="header__text"
          onClick={() =>
            setTimeout(() => {
              window.location.reload()
            }, 300)
          }
        >
          Title
        </p>
      </Link>
      <Button key="2">Post a job</Button>
    </div>
    <Link to={`/about`}>
      <Button key="1" type="primary">
        About
      </Button>
    </Link>
  </div>
)
export default Header
