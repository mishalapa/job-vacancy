import React, { useEffect, useState } from "react"
import { graphql, Link } from "gatsby"
import { Card, Select } from "antd"
import "antd/dist/antd.css"
import "./App.css"
import Header from "./Header"
const { Option } = Select
export const query = graphql`
  {
    allAirtable {
      nodes {
        data {
          company
          company_description
          date_post
          position
          position_description
          tags
          logo {
            url
          }
          framework
          grade
        }
      }
    }
  }
`

export default ({ data }) => {
  const [tags, setTags] = useState("")
  const [framework, setFramework] = useState("")
  const [grade, setGrade] = useState("")
  const [datas1, setDatas1] = useState("")
  const [firstSelect, setFirstSelect] = useState("")
  const [secondSelect, setSecondSelect] = useState("")
  const [thirdSelect, setThirdSelect] = useState("")
  const [anyChange, setAnyChange] = useState(false)

  useEffect(() => {
    if (!anyChange) {
      filterPostsTags()
    } else {
      filterPostsAll()
    }
  }, [tags])

  useEffect(() => {
    if (!anyChange) {
      filterPostsFramework()
    } else {
      filterPostsAll()
    }
  }, [framework])

  useEffect(() => {
    if (!anyChange) {
      filterPostsGrade()
    } else {
      filterPostsAll()
    }
  }, [grade])

  function filterPostsTags() {
    console.log("datas1 tags", datas1)
    let newData = ""

    if (tags !== "") {
      if (datas1) {
        if (firstSelect === tags && !framework && !grade) {
          return
        }
        if (firstSelect !== tags && !framework && !grade) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.tags === tags
          )
          setFirstSelect(tags)
        }
        if (grade && framework) {
          filterPostsAll()
          return
        } else if (framework) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.framework === framework
          )
          newData = newData.filter(node => node.data.tags === tags)
        } else if (grade) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.grade === grade
          )
          newData = newData.filter(node => node.data.tags === tags)
        }
      } else {
        newData = data.allAirtable.nodes.filter(node => node.data.tags === tags)
      }
      console.log(datas1)
      console.log("tags1", datas1)
      setDatas1(newData)

      console.log("tags2", datas1)
    }
    if (tags && framework && grade) {
      setAnyChange(true)
    }
  }

  function filterPostsFramework() {
    console.log("datas1 frmae", datas1)
    let newData = ""

    if (framework !== "") {
      if (datas1) {
        if (secondSelect === framework && !tags && !grade) {
          console.log("true")
          return
        }
        if (secondSelect !== framework && !tags && !grade) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.framework === framework
          )
          setSecondSelect(framework)
        }
        if (grade && tags) {
          filterPostsAll()
          return
        } else if (tags) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.tags === tags
          )
          newData = newData.filter(node => node.data.framework === framework)
        } else if (grade) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.grade === grade
          )
          newData = newData.filter(node => node.data.framework === framework)
        }
      } else {
        newData = data.allAirtable.nodes.filter(
          node => node.data.framework === framework
        )
      }
      setDatas1(newData)
    }
    if (tags && framework && grade) {
      setAnyChange(true)
    }
  }

  function filterPostsGrade() {
    console.log("datas1 grade", datas1)
    let newData = ""

    if (grade !== "") {
      if (datas1) {
        if (thirdSelect === grade && !framework && !tags) {
          return
        }
        if (thirdSelect !== grade && !tags && !framework) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.grade === grade
          )
          setThirdSelect(grade)
        }
        if (framework && tags) {
          console.log("papapam")
          filterPostsAll()
          return
        } else if (framework) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.framework === framework
          )
          newData = newData.filter(node => node.data.grade === grade)
        } else if (tags) {
          newData = data.allAirtable.nodes.filter(
            node => node.data.tags === tags
          )
          newData = newData.filter(node => node.data.grade === grade)
        }
      } else {
        newData = data.allAirtable.nodes.filter(
          node => node.data.grade === grade
        )
      }
      console.log(datas1)
      console.log("grade1", datas1)
      setDatas1(newData)
      console.log("grade2", datas1)
    }
    if (tags && framework && grade) {
      setAnyChange(true)
      console.log("aaa")
    }
  }

  function filterPostsAll() {
    console.log(tags, framework)
    console.log("alll gadme")

    let newData = ""
    newData = data.allAirtable.nodes.filter(
      node => node.data.framework === framework
    )
    console.log("newData all 1", newData)
    newData = newData.filter(node => node.data.tags === tags)
    console.log("newData all 2", newData)
    newData = newData.filter(node => node.data.grade === grade)
    console.log("newData all 3", newData)
    setDatas1(newData)
  }

  const arrayTags = []
  const arrayFramework = []
  const arrayGrade = []
  data.allAirtable.nodes.forEach(node => {
    arrayTags.push(node.data.tags)
    arrayFramework.push(node.data.framework)
    arrayGrade.push(node.data.grade)
  })
  const uniqSetTags = new Set(arrayTags)
  const uniqSetFramework = new Set(arrayFramework)
  const uniqSetGrade = new Set(arrayGrade)
  const newArrayTags = [...uniqSetTags]
  const newArrayFramework = [...uniqSetFramework]
  const newArrayGrade = [...uniqSetGrade]

  return (
    <>
      <Header />
      <div className="container">
        <h1>Golang jobs October 2022</h1>
        <Select
          className="select__post"
          onChange={value => {
            if (!tags) {
              setFirstSelect(value)
            }
            setTags(value)
          }}
          placeholder="Select a contry"
        >
          {newArrayTags.map(node => (
            <Option key={node}>{node}</Option>
          ))}
        </Select>

        <Select
          className="select__post"
          onChange={value => {
            if (!framework) {
              setSecondSelect(value)
            }
            setFramework(value)
          }}
          placeholder="Select a framework"
        >
          {newArrayFramework.map(node => (
            <Option key={node}>{node}</Option>
          ))}
        </Select>

        <Select
          className="select__post"
          onChange={value => {
            if (!grade) {
              setThirdSelect(value)
            }
            setGrade(value)
          }}
          placeholder="Select a grade"
        >
          {newArrayGrade.map(node => (
            <Option key={node}>{node}</Option>
          ))}
        </Select>
        <div>
          {tags === "" &&
            framework === "" &&
            grade === "" &&
            data.allAirtable.nodes.map(node => (
              <Card key={node.recordId} className="card__post">
                <div className="post">
                  <div className="post__logo">
                    <img className="photo" src={node.data.logo[0].url} alt="" />
                    <p className="photo__text">{node.data.company}</p>
                  </div>
                  <div>
                    <div className="header__post">
                      <Link to={`/${node.data.company}`}>
                        {node.data.position}
                      </Link>
                      <p>{node.data.date_post}</p>
                      <p>{node.data.framework}</p>
                      <p>{node.data.grade}</p>
                    </div>
                    <div className="footer__post">
                      <p>
                        {node.data.position_description.length < 100
                          ? node.data.position_description
                          : node.data.position_description.substring(0, 400) +
                            "..."}
                      </p>
                    </div>
                  </div>
                </div>
              </Card>
            ))}

          {datas1 !== "" &&
            datas1.map(node => (
              <Card key={node.recordId} className="card__post">
                <div className="post">
                  <div className="post__logo">
                    <img className="photo" src={node.data.logo[0].url} alt="" />
                    <p className="photo__text">{node.data.company}</p>
                  </div>
                  <div>
                    <div className="header__post">
                      <Link to={`/${node.data.company}`}>
                        {node.data.position}
                      </Link>
                      <p>{node.data.date_post}</p>
                      <p>{node.data.framework}</p>
                      <p>{node.data.grade}</p>
                    </div>
                    <div className="footer__post">
                      <p>{node.data.position_description}</p>
                    </div>
                  </div>
                </div>
              </Card>
            ))}
        </div>
      </div>
    </>
  )
}
