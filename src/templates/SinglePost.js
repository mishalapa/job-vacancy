import React from "react"

import { graphql, Link } from "gatsby"
import Card from "antd/lib/card/Card"
import Header from "../pages/Header"

const SinglePost = ({ data }) => {
  console.log("data2", data)
  return (
    <>
      <Header />
      <Card key={data.allAirtable.nodes[0].recordId} className="card__post">
        <h1>{data.allAirtable.nodes[0].data.position}</h1>
        <img
          className="photo__single"
          src={data.allAirtable.nodes[0].data.logo[0].url}
          alt=""
        />
        <div>
          <h2>Description</h2>
          <p>{data.allAirtable.nodes[0].data.position_description}</p>
        </div>
      </Card>
    </>
  )
}

export default SinglePost

export const query = graphql`
  query MyQuery($url: String) {
    allAirtable(filter: { data: { company: { eq: $url } } }) {
      nodes {
        recordId
        data {
          logo {
            url
          }
          position
          position_description
        }
      }
    }
  }
`
