const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const { data } = await graphql(`
    {
      allAirtable {
        nodes {
          data {
            company
          }
        }
      }
    }
  `)

  data.allAirtable.nodes.forEach(node => {
    const url = node.data.company

    createPage({
      path: `/${url}`,
      component: path.resolve("./src/templates/SinglePost.js"),
      context: { url },
    })
  })
}
